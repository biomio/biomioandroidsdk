package sdk.biomioandroid.demo;

import android.hardware.Camera;

import java.util.List;

public class CameraResolutionsUtil {

    public static final String TAG = CameraResolutionsUtil.class.getSimpleName();

    private static StringBuilder mStringBuilder = new StringBuilder();

    public CameraResolutionsUtil() {
    }


    /**
     * Returns String of available Front Camera resolutions
     *
     * @return - String
     */
    public static String getFrontCameraResolutionsString() {
        Camera camera = getFrontCamera();
        if (camera != null) {
            List<Camera.Size> cameraSizes = camera.getParameters().getSupportedPictureSizes();
            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;
            if (cameraSizes != null) {
                for (Camera.Size size : cameraSizes) {
                    mStringBuilder.append(",").append(size.width).append("x").append(size.height);
                }
            }
        }
        return mStringBuilder.toString();
    }

    /**
     * Returns String of available Back Camera resolutions
     *
     * @return - String
     */
    public static String getBackCameraResolutionsString() {
        mStringBuilder.setLength(0);
        Camera camera = Camera.open();
        if (camera != null) {
            List<Camera.Size> cameraSizes = camera.getParameters().getSupportedPictureSizes();

            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;

            if (cameraSizes != null) {
                for (Camera.Size size : cameraSizes) {
                    mStringBuilder.append(",").append(size.width).append("x").append(size.height);
                }
            }
        }
        return mStringBuilder.toString();
    }

    /**
     * Opens Front Camera object
     *
     * @return Camera
     */
    private static Camera getFrontCamera() {
        int cameraCount = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    cam = Camera.open(camIdx);
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }
        }
        return cam;
    }

    public interface OnCameraResolutionsFetchedListener {
        void onFetchedResolutions();
    }

}