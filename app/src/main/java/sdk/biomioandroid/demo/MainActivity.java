package sdk.biomioandroid.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.biomio.sdk.ui.BaseBiomioView;
import com.biomio.sdk.BiomioAndroidSDK;
import com.biomio.sdk.interfaces.OnAuthReadyListener;
import com.biomio.sdk.util.DigestUtil;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import javax.net.ssl.SSLContext;

import biomio.sdk.BiomioSDK;
import biomio.sdk.OnBiomioSdkListener;
import biomio.sdk.internal.Options;
import core.AbstractSocketCallManager;
import core.RpcModel;

/**
 * The example of SDK usage
 */
public class MainActivity
        extends AppCompatActivity
        implements View.OnClickListener, OnBiomioSdkListener, OnAuthReadyListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String KEY_REFRESH_TOKEN = "refreshToken";
    private static final String KEY_APP_FINGERPRINT = "appFingerprint";
    private static final String KEY_CONNECTION_TTL = "connectionTtl";
    private static final String KEY_RSA_PRIVATE_KEY = "rsaPrivateKey";

    private BiomioAndroidSDK mBiomioAndroidSDK;
    private BiomioSDK mBiomioJavaSdk;
    private AbstractSocketCallManager mAbstractSocketCallManager;

    private Button mButtonConnect;
    private Button mButtonRegisterDevice;
    private Button mButtonSendRPC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Pref.init(this);
        setContentView(R.layout.activity_main);
        try {
            mBiomioJavaSdk = BiomioSDK.initialize(SSLContext.getDefault(), "wss://gate-dev.biom.io:8080/websocket", createOptions());
            mBiomioJavaSdk.subscribe(this);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        mBiomioAndroidSDK = new BiomioAndroidSDK.Builder(this)
                .build();
        mBiomioAndroidSDK.setAuthReadyListener(this);
        if (mBiomioJavaSdk != null) {
            mBiomioJavaSdk.registerAuthListener(mBiomioAndroidSDK);
        }
        setupViews();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register_device:
                mBiomioAndroidSDK.startRegistration();
                break;
            case R.id.btn_connect:
                connect();
                break;
            case R.id.btn_logout:
                logout();
                if (mBiomioJavaSdk != null) {
                    mBiomioJavaSdk.disconnect();
                }
                break;
            case R.id.btn_rpc:
                if (mAbstractSocketCallManager != null) {
                    mAbstractSocketCallManager.sendRPC(createRpcAuthModel());
                }
                break;
        }
    }

    @Override
    public void onConnecting() {
        runOnUiThread(() -> mButtonConnect.setText("Connecting..."));
    }

    @Override
    public void onConnected(final AbstractSocketCallManager abstractSocketCallManager) {
        runOnUiThread(() -> {
            this.mAbstractSocketCallManager = abstractSocketCallManager;
            mButtonConnect.setText("Connected");
            mButtonConnect.setEnabled(false);
            if (isDeviceRegistered()) {
                abstractSocketCallManager.sendRegularHello(getAppFingerprint());
                setUiDeviceRegistered();
            }
        });
    }

    @Override
    public void onRegistrationHello(int connectionTtl,
                                    int sessionTtl,
                                    String refreshToken,
                                    String privateKey,
                                    String fingerPrint) {
        saveConnectionTtl(connectionTtl);
        saveRefreshToken(refreshToken);
        saveAppFingerprint(fingerPrint);
        saveKey(privateKey);
        setUiDeviceRegistered();
    }

    @Override
    public void onRegularHello(int connectionTtl,
                               int sessionTtl,
                               String refreshToken,
                               String fingerPrint,
                               String headerForDigest,
                               AbstractSocketCallManager abstractSocketCallManager) {
        saveConnectionTtl(connectionTtl);
        saveRefreshToken(refreshToken);
        abstractSocketCallManager.sendRegularHandShake(getAppFingerprint(), DigestUtil.generateDigest(getRsaPrivateKey(), headerForDigest));
    }

    @Override
    public void onResources(final AbstractSocketCallManager abstractSocketCallManager) {

    }

    @Override
    public void onTry(String s, AbstractSocketCallManager abstractSocketCallManager) {

    }

    @Override
    public void onProbeStatus(String s) {

    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onResponseStatus(final String s) {

    }

    @Override
    public void onRpcResponse(String s, AbstractSocketCallManager abstractSocketCallManager) {
        abstractSocketCallManager.sendRPC(createRpcAuthModel());
    }

    @Override
    public void onLog(String s, boolean b) {

    }

    private void connect() {
        if (mBiomioJavaSdk != null) {
            mBiomioJavaSdk.connect();
        }
    }

    private void logout() {
        saveKey("");
        saveRefreshToken("");
        saveConnectionTtl(0);
        saveAppFingerprint("");
        mBtnLogout.setEnabled(false);
        mButtonRegisterDevice.setEnabled(true);
        mButtonRegisterDevice.setText("Register Device");
    }

    private String getRefreshToken() {
        return Pref.getString(KEY_REFRESH_TOKEN);
    }

    private String getAppFingerprint() {
        return Pref.getString(KEY_APP_FINGERPRINT);
    }

    private String getRsaPrivateKey() {
        return Pref.getString(KEY_RSA_PRIVATE_KEY);
    }

    private int getConnectionTtl() {
        return Pref.getInt(KEY_CONNECTION_TTL);
    }

    private boolean isDeviceRegistered() {
        return !TextUtils.isEmpty(getAppFingerprint()) && !TextUtils.isEmpty(getRsaPrivateKey());
    }

    private void saveRefreshToken(String refreshToken) {
        Pref.setString(KEY_REFRESH_TOKEN, refreshToken);
    }

    private void saveAppFingerprint(String fingerprint) {
        Pref.setString(KEY_APP_FINGERPRINT, fingerprint);
    }

    private void saveKey(String key) {
        Pref.setString(KEY_RSA_PRIVATE_KEY, key);
    }

    private void saveConnectionTtl(int connectionTtl) {
        Pref.setInt(KEY_CONNECTION_TTL, connectionTtl);
    }

    @Override
    public void onDisconnected() {
        runOnUiThread(() -> {
            mButtonConnect.setEnabled(true);
            mButtonConnect.setText("Connect");
        });
    }

    private Button mBtnLogout;

    private void setupViews() {
        mButtonConnect = (Button) findViewById(R.id.btn_connect);
        mButtonConnect.setOnClickListener(this);

        mButtonRegisterDevice = (Button) findViewById(R.id.btn_register_device);
        mButtonRegisterDevice.setOnClickListener(this);

        mBtnLogout = (Button) findViewById(R.id.btn_logout);
        mBtnLogout.setOnClickListener(this);

        mButtonSendRPC = (Button) findViewById(R.id.btn_rpc);
        mButtonSendRPC.setOnClickListener(this);

        if (isDeviceRegistered()) {
            mButtonConnect.performClick();
        }
    }

    private void setUiDeviceRegistered() {
        runOnUiThread(() -> {
            if (isDeviceRegistered()) {
                mButtonRegisterDevice.setText("Device was registered");
                mButtonRegisterDevice.setEnabled(false);
                mBtnLogout.setVisibility(View.VISIBLE);
                mButtonSendRPC.setVisibility(View.VISIBLE);
            } else mBtnLogout.setVisibility(View.VISIBLE);
        });
    }

    private Options createOptions() {
        return new Options(
                "testAndroidDeviceId123",
                "Android",
                "Wifi",
                "1.0",
                getRefreshToken(),
                getAppFingerprint(),
                "",
                getConnectionTtl()
        );
    }

    @Override
    public void onAuthActivity(Intent startActivityIntent) {
        startActivity(startActivityIntent);
    }

    @Override
    public void onAutView(BaseBiomioView baseBiomioView) {

    }

    private RpcModel createRpcAuthModel() {
        RpcModel rpcModel = new RpcModel();
        rpcModel.setCall("process_auth");
        HashMap<String, Object> data = new HashMap<>();
        data.put("email", "vitaliy.herasymchuk@vakoms.com.ua");
        data.put("auth_code", "NO_REST");
        rpcModel.setData(data);
        rpcModel.setNameSpace("auth_client_plugin");
        rpcModel.setOid("rpcReq");
        rpcModel.setOnBehalfOf("vitaliy.herasymchuk@vakoms.com.ua");
        return rpcModel;
    }

    private RpcModel createRpcCheckUserExistsModel() {
        RpcModel rpcModel = new RpcModel();
        rpcModel.setCall("check_user_exists");
        HashMap<String, Object> data = new HashMap<>();
        data.put("client_key", "vitaliy.herasymchuk@vakoms.com.ua");
        rpcModel.setData(data);
        rpcModel.setNameSpace("auth_client_plugin");
        rpcModel.setOid("rpcReq");
        rpcModel.setOnBehalfOf("vitaliy.herasymchuk@vakoms.com.ua");
        return rpcModel;
    }
}
