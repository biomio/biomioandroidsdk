package com.biomio.sdk;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.biomio.sdk.ui.BaseBiomioView;
import com.biomio.sdk.mvp.BiomioPresenter;
import com.biomio.sdk.util.Const;
import com.biomio.sdk.util.LocationManager;
import com.biomio.sdk.ui.views.RegistrationView;

/**
 * This Activity represents the view which are responsible for authorization
 * and registration
 */
public class BiomioActivity extends BaseBiomioActivity {

    private static final String TAG = BiomioActivity.class.getSimpleName();

    public static final String ACTION_TYPE = "argActionType";
    public static final String ARG_TOOLBAR_COLOR = "argBiomioToolbarColor";
    public static final String ARG_STATUS_BAR_COLOR = "argBiomioStatusBarColor";
    public static final String ARG_TOOLBAR_TITLE = "argBiomioToolbarTitle";
    public static final String ARG_MSG_TRY_JSON_STRING = "argBiomioMsgTryStringJSON";

    public static final int ACTION_AUTHORIZE = 1;
    public static final int ACTION_REGISTER = 2;

    private Button mBtnCancel;
    private TextView mTvToolbarTitle;
    public static boolean isRunning = false;

    private RegistrationView mRegistrationView;

    private int mToolbarColor;
    private int mStatusBarColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biomio);
        getUiResources();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getCompatColor(mToolbarColor));
        mTvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        setStatusBarColor(getCompatColor(mStatusBarColor));
        FrameLayout container = (FrameLayout) findViewById(R.id.content);

        if (getActionType() == ACTION_AUTHORIZE) {
            BaseBiomioView baseBiomioView = new BaseBiomioView(this);
            baseBiomioView.setToolbarTitle(getToolbarTitle());
            container.addView(baseBiomioView);
            getAndDeliverMsgTryJsonStingToView();
        } else if (getActionType() == ACTION_REGISTER) {
            mRegistrationView = new RegistrationView(this);
            mRegistrationView.setDeviceRegisteredListener(new RegistrationView.OnDeviceRegisteredListener() {
                @Override
                public void onDeviceRegistered() {
                    Toast.makeText(BiomioActivity.this, getResources().getString(R.string.registered), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
            container.addView(mRegistrationView);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length == 0) return;
        int result = grantResults[0];
        if (requestCode == Const.PERMISSION_CAMERA_RC) {
            if (mRegistrationView != null) {
                mRegistrationView.onPermissionResult(result);
            }
        } else if (requestCode == LocationManager.RC_LOCATION_PERMISSION) {
            LocationManager.getInstance().onLocationPermission(BiomioActivity.this, result);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LocationManager.RC_GPS_SETTINGS) {
            LocationManager.getInstance().getLocation(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRunning = true;
        if (mRegistrationView != null) {
            mRegistrationView.resume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isRunning = false;
    }

    /**
     * Retrieves ui resources passed from client
     */
    private void getUiResources() {
        Intent intent = getIntent();
        mToolbarColor = intent.getIntExtra(ARG_TOOLBAR_COLOR, R.color.color_default);
        mStatusBarColor = intent.getIntExtra(ARG_STATUS_BAR_COLOR, R.color.color_default);
    }

    /**
     * Sets cancel button visibility
     * @param visibility - View.visibility
     */
    public void setCancelButtonVisibility(int visibility) {
        mBtnCancel.setVisibility(visibility);
    }

    /**
     * Sets cancel button click listener
     * @param listener - {@link android.view.View.OnClickListener}
     */
    public void setCancelButtonClickListener(View.OnClickListener listener) {
        mBtnCancel.setOnClickListener(listener);
    }

    /**
     * Sets toolbar title
     */
    public void setToolbarTitle(String title) {
        mTvToolbarTitle.setText(title);
    }

    /**
     * Retrieves action type from the {@link Intent}
     * @return - ACTION_TYPE
     */
    private int getActionType() {
        return getIntent().getExtras().getInt(ACTION_TYPE);
    }

    /**
     * Retrieves try string from intent
     */
    private void getAndDeliverMsgTryJsonStingToView() {
        String msgTryJsonString = getMsgTryJsonString();
        if (!TextUtils.isEmpty(msgTryJsonString)) {
            Intent intent = new Intent(BiomioPresenter.ACTION_AUTH);
            intent.putExtra(BiomioPresenter.ARG_AUTH_TRY, msgTryJsonString);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }

    /**
     * Retrieves try string from intent
     */
    private String getMsgTryJsonString() {
        if (getIntent() == null) return "";
        if (!getIntent().hasExtra(ARG_MSG_TRY_JSON_STRING)) return "";
        return getIntent().getExtras().getString(ARG_MSG_TRY_JSON_STRING);
    }

    /**
     * Retrieves toolbar title
     */
    private String getToolbarTitle() {
        if (getIntent() != null && getIntent().hasExtra(ARG_TOOLBAR_TITLE)) {
            return getIntent().getStringExtra(ARG_TOOLBAR_TITLE);
        }
        return "";
    }

    /**
     * Gets color
     */
    private int getCompatColor(int id) {
        return ContextCompat.getColor(this, id);
    }
}
