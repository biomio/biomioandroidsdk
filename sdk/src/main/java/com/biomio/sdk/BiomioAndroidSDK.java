package com.biomio.sdk;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.biomio.sdk.interfaces.OnAuthReadyListener;
import com.biomio.sdk.mvp.BiomioPresenter;
import com.biomio.sdk.util.Const;
import com.biomio.sdk.util.DeviceCapabilitiesUtil;
import com.biomio.sdk.util.Logger;
import com.biomio.sdk.ui.views.RegistrationView;

import core.AbstractSocketCallManager;
import core.BiomioAuthListener;
import core.Probe;

public class BiomioAndroidSDK implements BiomioAuthListener {

    private static final String TAG = BiomioAndroidSDK.class.getSimpleName();

    private BroadcastReceiver mReceiver;
    private OnAuthReadyListener mAuthReadyListener = null;
    private AbstractSocketCallManager mSocketCallManager = null;

    private Context mContext;
    private int mToolbarColor = -1;
    private int mStatusBarColor = -1;

    private BiomioAndroidSDK(Builder builder) {
        this.mContext = builder.context;
        this.mToolbarColor = builder.toolbarColor;
        this.mStatusBarColor = builder.statusBarColor;
        registerAuthResultReceiver();
    }

    @Override
    public void onSocketCallManager(AbstractSocketCallManager abstractSocketCallManager) {
        mSocketCallManager = abstractSocketCallManager;
        Logger.d(TAG, "onSocketCallManager :: socket call manager was received ");
    }

    @Override
    public void onTry(String s) {
        if (!BiomioActivity.isRunning) {
            constructAuthActivity(s);
        }
        notifyAuthResources(s);
    }

    @Override
    public void onProbeStatus(String s) {
        notifyProbeStatus(s);
    }

    @Override
    public void onResponseStatus(String s) {
        Logger.d(TAG, "onResponseStatus :: " + s);
        if (TextUtils.equals(s, "Device Registered")) {
            notifyDeviceRegistered();
        } else if (TextUtils.equals(s, "event probe inappropriate in current state ready")) {
            notifyProbeStatus("Received");
            //TODO for try simulator only
        }
    }

    @Override
    public void onResources() {
        if (mSocketCallManager != null) {
            mSocketCallManager.sendResources(DeviceCapabilitiesUtil.generateDeviceCapabilitiesMap(mContext), "");
            Logger.d(TAG, "onResources :: sending resources");
        }
    }

    public void release() {
        mAuthReadyListener = null;
        LocalBroadcastManager
                .getInstance(mContext)
                .unregisterReceiver(mReceiver);
    }

    public void startRegistration() {
        Intent intent = new Intent(mContext, BiomioActivity.class);
        intent.putExtra(BiomioActivity.ACTION_TYPE, BiomioActivity.ACTION_REGISTER);
        mContext.startActivity(intent);
    }

    private void registerAuthResultReceiver() {
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (mSocketCallManager != null && intent != null) {
                    if (intent.hasExtra(Const.ARG_PROBE)) {
                        Probe probe = (Probe) intent.getSerializableExtra(Const.ARG_PROBE);
                        Logger.d(TAG, "onReceive :: received auth result " + probe.getStatus());
                        mSocketCallManager.sendProbe(probe);
                    }
                    if (intent.hasExtra(Const.ARG_SECRET)) {
                        String secret = intent.getStringExtra(Const.ARG_SECRET);
                        mSocketCallManager.sendClientHello(secret);
                    }
                }
            }
        };

        LocalBroadcastManager.getInstance(mContext)
                .registerReceiver(mReceiver, new IntentFilter(Const.ACTION_AUTH_RESULT));
    }

    private void notifyProbeStatus(String status) {
        Intent intent = new Intent(BiomioPresenter.ACTION_AUTH);
        intent.putExtra(BiomioPresenter.ARG_PROBE_STATUS, status);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        Logger.d(TAG, "notifyProbeStatus :: notified");
    }

    private void notifyAuthResources(String jsonString) {
        Intent intent = new Intent(BiomioPresenter.ACTION_AUTH);
        intent.putExtra(BiomioPresenter.ARG_AUTH_TRY, jsonString);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        Logger.d(TAG, "notifyAuthResources :: notified");
    }

    private void notifyDeviceRegistered() {
        Intent intent = new Intent(Const.ACTION_AUTH_RESULT);
        intent.putExtra(RegistrationView.ARG_DEVICE_REGISTERED, true);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

    private void constructAuthActivity(String msgTryJSON) {
        if (mAuthReadyListener != null) {
            Intent intent = new Intent(mContext, BiomioActivity.class);
            intent.putExtra(BiomioActivity.ACTION_TYPE, BiomioActivity.ACTION_AUTHORIZE);
            if (mToolbarColor != -1) {
                intent.putExtra(BiomioActivity.ARG_TOOLBAR_COLOR, mToolbarColor);
            }
            if (mStatusBarColor != -1) {
                intent.putExtra(BiomioActivity.ARG_STATUS_BAR_COLOR, mStatusBarColor);
            }
            intent.putExtra(BiomioActivity.ARG_MSG_TRY_JSON_STRING, msgTryJSON);
            mAuthReadyListener.onAuthActivity(intent);
        }
    }

    public void setAuthReadyListener(OnAuthReadyListener listener) {
        this.mAuthReadyListener = listener;
    }

    public static class Builder {

        private Context context;
        private int toolbarColor = -1;
        private int statusBarColor = -1;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder toolbarColor(int toolbarColor) {
            this.toolbarColor = toolbarColor;
            return this;
        }

        public Builder statusBarColor(int statusBarColor) {
            this.statusBarColor = statusBarColor;
            return this;
        }


        public BiomioAndroidSDK build() {
            return new BiomioAndroidSDK(this);
        }
    }
}
