package com.biomio.sdk.interfaces;


public interface OnQrListener {

    void onQrDetected(String result);

}
