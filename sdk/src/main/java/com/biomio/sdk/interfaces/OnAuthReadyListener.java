package com.biomio.sdk.interfaces;


import android.content.Intent;

import com.biomio.sdk.ui.BaseBiomioView;

public interface OnAuthReadyListener {
    void onAuthActivity(Intent startActivityIntent);

    void onAutView(BaseBiomioView baseBiomioView);
}
