package com.biomio.sdk.model;


import java.util.ArrayList;
import java.util.List;

public class MsgTry {

    private List<AuthRes> resources = new ArrayList<>();

    private int authTimeout;
    private String oid;
    private String tryId;
    private String toolbarTittle;
    private String message;
    private String startAuthButtonMsg;
    private String condition;


    public int getAuthTimeout() {
        return authTimeout;
    }

    public void setAuthTimeout(int authTimeout) {
        this.authTimeout = authTimeout;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getTryId() {
        return tryId;
    }

    public void setTryId(String tryId) {
        this.tryId = tryId;
    }

    public String getToolbarTittle() {
        return toolbarTittle;
    }

    public void setToolbarTittle(String toolbarTittle) {
        this.toolbarTittle = toolbarTittle;
    }

    public List<AuthRes> getResources() {
        return resources;
    }

    public void setResources(List<AuthRes> resources) {
        this.resources = resources;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStartAuthButtonMsg() {
        return startAuthButtonMsg;
    }

    public void setStartAuthButtonMsg(String startAuthButtonMsg) {
        this.startAuthButtonMsg = startAuthButtonMsg;
    }

    @Override
    public String toString() {
        return "MsgTry{" +
                "resources=" + resources +
                ", authTimeout=" + authTimeout +
                ", oid='" + oid + '\'' +
                ", tryId='" + tryId + '\'' +
                ", toolbarTittle='" + toolbarTittle + '\'' +
                ", message='" + message + '\'' +
                ", startAuthButtonMsg='" + startAuthButtonMsg + '\'' +
                ", condition='" + condition + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MsgTry msgTry = (MsgTry) o;

        if (authTimeout != msgTry.authTimeout) return false;
        if (resources != null ? !resources.equals(msgTry.resources) : msgTry.resources != null)
            return false;
        if (oid != null ? !oid.equals(msgTry.oid) : msgTry.oid != null) return false;
        if (tryId != null ? !tryId.equals(msgTry.tryId) : msgTry.tryId != null) return false;
        if (toolbarTittle != null ? !toolbarTittle.equals(msgTry.toolbarTittle) : msgTry.toolbarTittle != null)
            return false;
        if (message != null ? !message.equals(msgTry.message) : msgTry.message != null)
            return false;
        if (startAuthButtonMsg != null ? !startAuthButtonMsg.equals(msgTry.startAuthButtonMsg) : msgTry.startAuthButtonMsg != null)
            return false;
        return condition != null ? condition.equals(msgTry.condition) : msgTry.condition == null;

    }

    @Override
    public int hashCode() {
        int result = resources != null ? resources.hashCode() : 0;
        result = 31 * result + authTimeout;
        result = 31 * result + (oid != null ? oid.hashCode() : 0);
        result = 31 * result + (tryId != null ? tryId.hashCode() : 0);
        result = 31 * result + (toolbarTittle != null ? toolbarTittle.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (startAuthButtonMsg != null ? startAuthButtonMsg.hashCode() : 0);
        result = 31 * result + (condition != null ? condition.hashCode() : 0);
        return result;
    }
}
