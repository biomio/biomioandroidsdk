package com.biomio.sdk.model;


import java.util.Date;

public class AuthRes {
    private String tryType;
    private String resourceType;
    private String tryId;
    private String properties;
    private int samples;
    private int timeout;

    private Date createdDate;

    private boolean selected;
    private boolean passed;
    private boolean active;


    public String getTryType() {
        return tryType;
    }

    public void setTryType(String tryType) {
        this.tryType = tryType;
    }

    public int getSamples() {
        return samples;
    }

    public void setSamples(int samples) {
        this.samples = samples;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getTryId() {
        return tryId;
    }

    public void setTryId(String tryId) {
        this.tryId = tryId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthRes res = (AuthRes) o;

        if (samples != res.samples) return false;
        if (timeout != res.timeout) return false;
        if (selected != res.selected) return false;
        if (passed != res.passed) return false;
        if (active != res.active) return false;
        if (tryType != null ? !tryType.equals(res.tryType) : res.tryType != null) return false;
        if (resourceType != null ? !resourceType.equals(res.resourceType) : res.resourceType != null)
            return false;
        if (tryId != null ? !tryId.equals(res.tryId) : res.tryId != null) return false;
        if (properties != null ? !properties.equals(res.properties) : res.properties != null)
            return false;
        return createdDate != null ? createdDate.equals(res.createdDate) : res.createdDate == null;

    }

    @Override
    public int hashCode() {
        int result = tryType != null ? tryType.hashCode() : 0;
        result = 31 * result + (resourceType != null ? resourceType.hashCode() : 0);
        result = 31 * result + (tryId != null ? tryId.hashCode() : 0);
        result = 31 * result + (properties != null ? properties.hashCode() : 0);
        result = 31 * result + samples;
        result = 31 * result + timeout;
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (selected ? 1 : 0);
        result = 31 * result + (passed ? 1 : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AuthRes{" +
                "tryType='" + tryType + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", tryId='" + tryId + '\'' +
                ", properties='" + properties + '\'' +
                ", samples=" + samples +
                ", timeout=" + timeout +
                ", createdDate=" + createdDate +
                ", selected=" + selected +
                ", passed=" + passed +
                ", active=" + active +
                '}';
    }
}
