package com.biomio.sdk.model;


import java.util.UUID;

public class AuthType {

    private String id = UUID.randomUUID().toString();
    private String type;

    private boolean selected;
    private boolean passed;
    private boolean active;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthType authType = (AuthType) o;

        if (selected != authType.selected) return false;
        if (passed != authType.passed) return false;
        if (active != authType.active) return false;
        if (id != null ? !id.equals(authType.id) : authType.id != null) return false;
        return type != null ? type.equals(authType.type) : authType.type == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (selected ? 1 : 0);
        result = 31 * result + (passed ? 1 : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }
}
