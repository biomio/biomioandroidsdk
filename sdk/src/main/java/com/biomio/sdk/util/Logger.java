package com.biomio.sdk.util;

import android.util.Log;


public class Logger {

    private static final boolean DEBUG = true;

    public static void i(String text, String message) {
        if (DEBUG) {
            Log.i(text, message);
        }
    }

    public static void w(String text, String message) {
        if (DEBUG) {
            Log.w(text, message);
        }
    }

    public static void e(String text, String message) {
        e(text, message, null);
    }

    public static <EXC extends Throwable> void e(String text, String message, EXC e) {
        if (DEBUG) {
            if (e == null) Log.e(text, message);
            else Log.e(text, message, e);
        }
    }

    public static void d(String text, String message) {
        if (DEBUG) {
            Log.d(text, message);
        }
    }


}
