package com.biomio.sdk.util;

/**
 * Constants
 */
public class Const {

    public static final String AUTH_FACE = "face";
    public static final String AUTH_PUSH = "push_button";
    public static final String AUTH_FP = "fp";
    public static final String AUTH_PIN_CODE = "pin_code";
    public static final String AUTH_CREDIT_CARD = "credit_card";
    public static final String AUTH_LOCATION = "location";

    public static String ACTION_AUTH_RESULT = "action_auth_result";
    public static String ARG_PROBE = "action_auth_result";
    public static String ARG_SECRET = "argSecret";

    public static final String LOCATION_SAMPLES = "locationSamples";
    public static final String IMAGE_SAMPLES = "imageSamples";
    public static final String PIN_CODE_SAMPLES = "pincodeSamples";
    public static final String TOUCH_ID_SAMPLES = "touchIdSamples";
    public static final String CANCELED = "canceled";

    public static final String CONDITION_ANY = "any";
    public static final int PERMISSION_CAMERA_RC = 12;

    static final int ANIM_DURATION = 1500;
    static final String FRONT_CAMERA = "front-cam";
    static final String BACK_CAMERA = "back-cam";
    static final String MIC = "mic";
    static final String FP_SCANNER = "fp-scanner";
    static final String LOCATION = "location";


}
