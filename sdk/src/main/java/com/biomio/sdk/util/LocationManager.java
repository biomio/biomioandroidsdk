package com.biomio.sdk.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

/**
 * This class is responsible for retrieving location
 */
public class LocationManager implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static final int RC_GPS_SETTINGS = 2;
    public static final int RC_LOCATION_PERMISSION = 9;
    private static final String TAG = LocationManager.class.getSimpleName();
    private GoogleApiClient mClient;
    private OnLocationListener mListener;

    private static class Holder {
        static final LocationManager INSTANCE = new LocationManager();
    }

    public static LocationManager getInstance() {
        return Holder.INSTANCE;
    }

    private LocationManager() {

    }

    /**
     * Indicates that {@link GoogleApiClient} was successfully connected to Play Services
     *
     * @param bundle {@link Bundle}
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Logger.d(TAG, "onConnected");
        if (mListener != null) {
            mListener.onClientConnected();
        }
    }

    /**
     * Indicates that connection with Play Services was suspended
     *
     * @param i int
     */
    @Override
    public void onConnectionSuspended(int i) {
        Logger.d(TAG, "onConnectionSuspended");
    }

    /**
     * Indicates that connection with Play Services failed
     *
     * @param connectionResult {@link ConnectionResult}
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Logger.d(TAG, "onConnectionFailed");
    }

    /**
     * Indicates that location changed
     *
     * @param location {@link Location}
     */
    @Override
    public void onLocationChanged(Location location) {
        if (mListener != null) {
            if (location != null) {
                mListener.onLocation(location);
                LocationServices.FusedLocationApi.removeLocationUpdates(mClient, LocationManager.this);
            } else mListener.onLocationFail();
        }
    }

    /**
     * This method must be called in Activity's onRequestPermissionResult callback in order
     * to check whether user has granted the location permissions
     *
     * @param context {@link Context}
     * @param result  int representation of permission result
     */
    public void onLocationPermission(Context context, int result) {
        if (result == PackageManager.PERMISSION_GRANTED) {
            getLocation(context);
        } else {
            if (mListener != null) {
                mListener.onLocationFail();
            }
        }
    }

    /**
     * Starts the LocationManager by registering {@link OnLocationListener} and connection to
     * Play Services
     *
     * @param context          {@link Context}
     * @param locationListener {@link OnLocationListener}
     */
    public void start(Context context, OnLocationListener locationListener) {
        this.mListener = locationListener;
        setupGoogleApiClient(context);
        Logger.d(TAG, "stop :: setupGoogleApiClient start");
    }


    /**
     * Stops the {@link LocationManager} by disconnecting from Play Services and unregistering
     * the {@link OnLocationListener}
     */
    public void stop() {
        if (mClient != null) mClient.disconnect();
        mListener = null;
        Logger.d(TAG, "stop :: disconnected");
    }

    /**
     * Tries to get {@link Location} by firstly checking the permissions.
     * If the last known location is missing for app it requests the high accuracy location
     *
     * @param context {@link Context}
     */
    public void getLocation(final Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission(context);
            return;
        }

        if (!isLocationEnabled(context)) {
            showLocationNotEnabledDialog(context);
            return;
        }

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mClient);
        if (mLastLocation != null) {
            mListener.onLocation(mLastLocation);
        } else {
            getHighAccuracyLocation(context);
        }
    }

    /**
     * Setups {@link GoogleApiClient}
     *
     * @param context {@link Context}
     */
    private void setupGoogleApiClient(Context context) {
        mClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mClient.connect();
    }

    /**
     * Checks whether the location is turned on
     *
     * @param context {@link Context}
     * @return true if it's enabled
     */
    private boolean isLocationEnabled(Context context) {
        int locationMode;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    /**
     * Shows the Location disabled alert
     *
     * @param context {@link Context}
     */
    private void showLocationNotEnabledDialog(final Context context) {
        new AlertDialog.Builder(context)
                .setTitle("GPS disabled")
                .setMessage("GPS is disabled. Would you like to turn it on?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Activity activity = (Activity) context;
                        activity.startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), RC_GPS_SETTINGS);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (mListener != null) {
                            mListener.onLocationFail();
                        }
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Retrieves the high accuracy location
     *
     * @param context {@link Context}
     */
    private void getHighAccuracyLocation(final Context context) {
        final LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        final PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            requestLocationPermission(context);
                            return;
                        }
                        LocationServices.FusedLocationApi.requestLocationUpdates(mClient, mLocationRequest, LocationManager.this);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        showLocationNotEnabledDialog(context);
                        break;
                }
            }
        });
    }

    /**
     * Requests the location permission in runtime
     *
     * @param context {@link Context}
     */
    private void requestLocationPermission(Context context) {
        ActivityCompat.requestPermissions((Activity) context,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, RC_LOCATION_PERMISSION);
    }

    /**
     * This listener indicates 3 events
     * 1 - {@link GoogleApiClient} connected event
     * 2 - Location retrieval successful event
     * 3 - Location retrieval failure event
     */
    public interface OnLocationListener {
        void onClientConnected();

        void onLocation(Location location);

        void onLocationFail();
    }


}
