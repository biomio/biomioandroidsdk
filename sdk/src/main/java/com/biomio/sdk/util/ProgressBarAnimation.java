package com.biomio.sdk.util;

import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ProgressBar;

/**
 * This class is responsible for progress bar's animation
 */
public class ProgressBarAnimation extends Animation {

    private ProgressBar mProgressBar;
    private float mFrom;
    private float mTo;

    public ProgressBarAnimation(ProgressBar progressBar) {
        super();
        this.mProgressBar = progressBar;
        setInterpolator(new AccelerateDecelerateInterpolator());
    }

    public void set(float from, float to) {
        this.mFrom = from;
        this.mTo = to;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        float value = mFrom + (mTo - mFrom) * interpolatedTime;
        mProgressBar.setProgress((int) value);
    }

}