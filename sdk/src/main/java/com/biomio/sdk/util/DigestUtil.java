package com.biomio.sdk.util;

import android.util.Base64;

import org.spongycastle.jce.provider.BouncyCastleProvider;
import org.spongycastle.util.encoders.Hex;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

/**
 * Class responsible for creating string-digest using PKCS1 and SHA1.
 * Uses Base64 String private RSA key instead of certificate-file
 */
public class DigestUtil {

	public static final String TAG = DigestUtil.class.getSimpleName();

	public static final String KEY_REPLACEMENT = "-----KEY-----";
    public static final String PUSH_TOKEN_REPLACEMENT = "-----PUSH_TOKEN-----";
	public static final String PUSH_TOKEN_START = "push_token\":\"";
	public static final String BEGIN_RSA = "-----BEGIN RSA PRIVATE KEY-----";
	public static final String END_RSA = "-----END RSA PRIVATE KEY-----";

	private static final String RSA = "RSA";
	private static final String PROVIDER = "BC";
	private static final String ENCODING_UTF8 = "UTF-8";
	private static final String ALGORITHM = "SHA1withRSA";

	//Enables BC provider
	static {
		Security.insertProviderAt(new BouncyCastleProvider(), 1);
	}

	/**
	 * Sign a string-digest using PKCS1 v1.5 and SHA1
	 *
	 * @param privateKeyStr - String private RSA key
	 * @param header        - Header.java string
	 * @return - String
	 */
	public static String generateDigest(String privateKeyStr, String header) {

		privateKeyStr = privateKeyStr.replace(BEGIN_RSA, "").replace(END_RSA, "");

		byte[] keyBytes = Base64.decode(privateKeyStr, Base64.DEFAULT);

		try {

			Signature signer = Signature.getInstance(ALGORITHM);
			signer.initSign(getPrivateKey(keyBytes));
			signer.update(header.getBytes(ENCODING_UTF8));

			return new String(Hex.encode(signer.sign()), ENCODING_UTF8);
		} catch (Exception e) {
			Logger.e(TAG, "Error occurred while creating digest " + e.getMessage());
		}

		return "";
	}

	/**
	 * Returns PrivateKey object from byte array of key RSA String
	 *
	 * @param keyBytes - byte[] of key String
	 * @return - PrivateKey
	 * @throws NoSuchProviderException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	private static PrivateKey getPrivateKey(byte[] keyBytes) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException {
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
		return KeyFactory.getInstance(RSA, PROVIDER).generatePrivate(keySpec);
	}

}