package com.biomio.sdk.mvp.view;


import android.content.Context;
import android.view.View;

import com.biomio.sdk.ui.camera.BiomioCamera;

public interface BiomioView {

    void addCameraView(BiomioCamera camera);

    void addAuthTypeView(View view);

    void setBannerVisibility(int visibility);

    void setAuthOptionsViewVisibility(int visibility);

    void invalidatePreviousAuthViews();

    void setCancelButtonVisibility(int visibility);

    void setStartTextViewVisibility(int visibility);

    void setPassiveRecyclerViewVisibility(int visibility);

    void setToolbarTitle(String title);

    void setStartActionMessage(String msg);

    void setMessage(String msg);

    Context getViewContext();
}
