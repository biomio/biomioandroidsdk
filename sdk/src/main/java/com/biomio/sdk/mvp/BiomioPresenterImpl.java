package com.biomio.sdk.mvp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.biomio.sdk.R;
import com.biomio.sdk.adapter.AuthOptionsAdapter;
import com.biomio.sdk.model.AuthRes;
import com.biomio.sdk.model.MsgTry;
import com.biomio.sdk.mvp.view.BiomioView;
import com.biomio.sdk.ui.camera.BiomioCamera;
import com.biomio.sdk.ui.camera.attrs.CameraFacing;
import com.biomio.sdk.ui.camera.attrs.DetectionType;
import com.biomio.sdk.ui.detector.OnFaceDetectorListener;
import com.biomio.sdk.ui.fingerprint.FingerprintFragmentDialog;
import com.biomio.sdk.ui.views.CircleView;
import com.biomio.sdk.ui.views.PinCodeView;
import com.biomio.sdk.ui.views.PushView;
import com.biomio.sdk.util.Const;
import com.biomio.sdk.util.LocationManager;
import com.biomio.sdk.util.Logger;
import com.biomio.sdk.util.ProgressBarAnimation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import core.Probe;
import core.ProbeData;

/**
 * This class is used to handle authentication
 */
public class BiomioPresenterImpl
        implements BiomioPresenter, LocationManager.OnLocationListener {

    private static final String TAG = BiomioPresenterImpl.class.getSimpleName();
    private static final int PROGRESS_MAX = 1000;
    private static final int PROGRESS_ANIM_DURATION = 400;

    private BiomioView mView;
    private AuthRes mCurrentAuthType = null;

    private AuthOptionsAdapter mActiveAdapter;
    private AuthOptionsAdapter mPassiveAdapter;

    private BroadcastReceiver mAuthReceiver;

    private String mCurrentCondition = "";
    private int mTakenSamplesCount = 0;
    private boolean authStarted = false;

    public BiomioPresenterImpl(AuthOptionsAdapter adapterActive, AuthOptionsAdapter adapterPassive, BiomioView view) {
        this.mView = view;
        this.mActiveAdapter = adapterActive;
        this.mPassiveAdapter = adapterPassive;
        adapterActive.setAuthTypeSelectedListener(this);
        adapterPassive.setAuthTypeSelectedListener(this);
        registerAuthReceiver();
    }

    /**
     * Called when app reaches resumed state
     */
    @Override
    public void onResume() {
        Logger.d(TAG, "onResume :: resumed");
    }

    /**
     * Called when app reaches destroyed state
     */
    @Override
    public void onDestroy() {
        release();
        mView = null;
        LocationManager.getInstance().stop();
    }

    /**
     * Starts authentication
     */
    @Override
    public void startAuth() {
        if (mView == null) return;
        if (!authStarted) {
            authStarted = true;
            selectAuthOption();
        }
    }

    /**
     * Removes desired {@link AuthRes} item from adapter
     *
     * @param item {@link AuthRes}
     */
    @Override
    public void removeItemFromAdapter(AuthRes item) {
        if (item == null) return;
        if (item.isActive()) {
            mActiveAdapter.removeItem(item);
        } else {
            mPassiveAdapter.removeItem(item);
        }
    }

    /**
     * Finished the auth by cleaning up all the resources and adapters
     */
    @Override
    public void finishAuth() {
        if (mView == null) return;
        authStarted = false;
        mActiveAdapter.getDataList().clear();
        mPassiveAdapter.getDataList().clear();
        mActiveAdapter.notifyDataSetChanged();
        mPassiveAdapter.notifyDataSetChanged();

        mView.invalidatePreviousAuthViews();
        mView.setCancelButtonVisibility(View.GONE);
        mView.setStartTextViewVisibility(View.GONE);
        mView.setBannerVisibility(View.VISIBLE);
        mView.setMessage("");
        mView.setToolbarTitle(mView.getViewContext().getResources().getString(R.string.biomio));
        mView.setStartTextViewVisibility(View.VISIBLE);
        mView.setPassiveRecyclerViewVisibility(View.GONE);
        Activity activity = (Activity) mView.getViewContext();
        activity.finish();
    }

    /**
     * Registers auth tries BroadcastReceiver
     */
    @Override
    public void registerAuthReceiver() {
        if (mView == null) return;
        mAuthReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (mView == null) return;
                Logger.d(TAG, "auth received");
                if (intent == null) return;
                if (intent.hasExtra(ARG_AUTH_TRY)) {
                    mView.setAuthOptionsViewVisibility(View.VISIBLE);
                    String jsonString = intent.getStringExtra(ARG_AUTH_TRY);
                    MsgTry msgTry = parseTryMessage(jsonString);

                    if (msgTry != null) {
                        mCurrentCondition = msgTry.getCondition();

                        if (!TextUtils.isEmpty(msgTry.getToolbarTittle())) {
                            mView.setToolbarTitle(msgTry.getToolbarTittle());
                        }

                        if (!TextUtils.isEmpty(msgTry.getStartAuthButtonMsg())) {
                            mView.setStartActionMessage(msgTry.getStartAuthButtonMsg());
                        }

                        if (!TextUtils.isEmpty(msgTry.getMessage())) {
                            mView.setMessage(msgTry.getMessage());
                        }

                        for (AuthRes res : msgTry.getResources()) {
                            Logger.d(TAG, res.toString());
                            if (!TextUtils.equals(res.getTryType(), Const.AUTH_CREDIT_CARD)) {
                                res.setActive(true);
                                res.setCreatedDate(new Date());
                                res.setTimeout(msgTry.getAuthTimeout());
                                res.setTryId(msgTry.getTryId());
                                if (TextUtils.equals(res.getResourceType(), Const.AUTH_LOCATION)) {
                                    res.setActive(false);
                                    mPassiveAdapter.addItem(res);
                                } else mActiveAdapter.addItem(res);
                            }
                        }
                    }
                }
                if (intent.hasExtra(ARG_PROBE_STATUS)) {
                    String status = intent.getStringExtra(ARG_PROBE_STATUS);
                    if (!TextUtils.equals(status, Const.CANCELED)) {
                        selectAuthOption();
                    } else {
                        Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
                        finishAuth();
                    }
                    Logger.d(TAG, "status " + status);
                }
            }
        };

        LocalBroadcastManager.getInstance(mView.getViewContext()).registerReceiver(mAuthReceiver,
                new IntentFilter(ACTION_AUTH));
        Logger.d(TAG, "registerAuthReceiver :: registered");
    }

    /**
     * Sends Intent with authentication Probe results
     *
     * @param probe - {@link Probe}
     */
    @Override
    public void sendAuthResultProbe(Probe probe) {
        Intent intent = new Intent(Const.ACTION_AUTH_RESULT);
        intent.putExtra(Const.ARG_PROBE, probe);
        LocalBroadcastManager.getInstance(mView.getViewContext()).sendBroadcast(intent);
        Logger.d(TAG, "sendAuthResultProbe :: auth result was sent");
    }

    /**
     * Releases resources used by presenter
     */
    @Override
    public void release() {
        if (mView == null) return;
        LocalBroadcastManager.getInstance(mView.getViewContext()).unregisterReceiver(mAuthReceiver);
        Logger.d(TAG, "release :: released");
    }

    /**
     * Handles try resources click event
     *
     * @param authRes {@link AuthRes}
     */
    @Override
    public void onAuthTypeSelected(final AuthRes authRes) {
        mView.invalidatePreviousAuthViews();
        mCurrentAuthType = authRes;
        mCurrentAuthType.setSelected(true);
        mView.setStartTextViewVisibility(View.GONE);
        mView.setPassiveRecyclerViewVisibility(View.VISIBLE);
        if (mCurrentAuthType.isActive()) {
            mActiveAdapter.itemChanged(mCurrentAuthType);
        } else {
            mPassiveAdapter.itemChanged(mCurrentAuthType);
        }

        if (TextUtils.equals(authRes.getTryType(), Const.AUTH_FACE)) {
            startFaceAuth(authRes);
        } else if (TextUtils.equals(authRes.getTryType(), Const.AUTH_FP)) {
            startFpAuth(authRes);
        } else if (TextUtils.equals(authRes.getTryType(), Const.AUTH_PUSH)) {
            startPushButtonAuth(authRes);
        } else if (TextUtils.equals(authRes.getTryType(), Const.AUTH_PIN_CODE)) {
            startPinAuth(authRes);
        } else if (TextUtils.equals(authRes.getTryType(), Const.AUTH_LOCATION)) {
            LocationManager.getInstance().start(mView.getViewContext(), this);
        }

        mView.setBannerVisibility(View.GONE);
    }


    /**
     * This callback is responsible for GoogleApiClient's connected event
     */
    @Override
    public void onClientConnected() {
        LocationManager.getInstance().getLocation(mView.getViewContext());
    }

    /**
     * This callback is responsible for location detected event
     *
     * @param location {@link Location}
     */
    @Override
    public void onLocation(Location location) {
        if (location != null) {
            String sample = location.getLatitude() + "," + location.getLatitude() + "," + location.getAccuracy();
            Toast.makeText(mView.getViewContext(), "location detected " + sample, Toast.LENGTH_SHORT).show();
            sendAuthResultProbe(createProbe(mCurrentAuthType.getTryId(), mCurrentAuthType.getTryType(), Const.LOCATION_SAMPLES, sample));
        }
    }

    /**
     * This callback is responsible for location failure event
     */
    @Override
    public void onLocationFail() {
        sendCanceledProbe();
    }

    /**
     * Sends "canceled" {@link Probe} to Gate
     */
    @Override
    public void sendCanceledProbe() {
        String id = "";
        String authType = "";
        if (mCurrentAuthType != null) {
            id = mCurrentAuthType.getTryId();
            authType = mCurrentAuthType.getTryType();
        }
        sendAuthResultProbe(createProbe(id, authType, "Probe", "canceled"));
    }

    /**
     * Selects next auth option automatically and starts authentication with next {@link AuthRes}
     */
    private void selectAuthOption() {
        if (TextUtils.equals(mCurrentCondition, Const.CONDITION_ANY) && mCurrentAuthType != null) {
            finishAuth();
            return;
        }

        removeItemFromAdapter(mCurrentAuthType);

        if (mActiveAdapter.getDataList().size() > 0) {
            AuthRes type = mActiveAdapter.getDataList().get(0);
            onAuthTypeSelected(type);
        } else if (mPassiveAdapter.getDataList().size() > 0) {
            AuthRes res = mPassiveAdapter.getDataList().get(0);
            onAuthTypeSelected(res);
        } else {
            finishAuth();
        }
    }

    /**
     * Starts the face authentication
     *
     * @param res - {@link AuthRes}
     */
    private void startFaceAuth(final AuthRes res) {
        mTakenSamplesCount = 0;
        checkForCameraPermission();
        final int samplesCount = res.getSamples();
        final BiomioCamera cameraView = new BiomioCamera(mView.getViewContext());
        cameraView.setDetectionType(DetectionType.FACE);
        cameraView.setCameraFacing(CameraFacing.FRONT);
        cameraView.setDrawFaceRect(false);
        cameraView.setSamplesCount(samplesCount);

        final View faceDetectionLayout = LayoutInflater.from(mView.getViewContext()).inflate(R.layout.face_detection_layout, null);
        final ProgressBar progressBar = (ProgressBar) faceDetectionLayout.findViewById(R.id.pb);
        progressBar.setMax(PROGRESS_MAX);
        final ProgressBarAnimation mAnimator = new ProgressBarAnimation(progressBar);
        mAnimator.setDuration(PROGRESS_ANIM_DURATION);

        final TextView tvSamplesTaken = (TextView) faceDetectionLayout.findViewById(R.id.tv_samples_taken);
        tvSamplesTaken.setText(0 + "/" + samplesCount);
        final ImageView imageView = (ImageView) faceDetectionLayout.findViewById(R.id.iv_face_state);

        mView.addAuthTypeView(faceDetectionLayout);
        mView.addCameraView(cameraView);

        imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                cameraView.setCameraVisibleRectWidth(imageView.getWidth());
                cameraView.setCameraVisibleRectHeight(imageView.getHeight() + dpToPx(130));
            }
        });

        cameraView.setFaceDetectorListener(new OnFaceDetectorListener() {
            @Override
            public void onFaceDetected(final String sample) {
                if (mView == null) return;
                Activity activity = (Activity) mView.getViewContext();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mTakenSamplesCount < samplesCount) {
                            mTakenSamplesCount += 1;
                            tvSamplesTaken.setText(mTakenSamplesCount + "/" + samplesCount);
                            int k = PROGRESS_MAX / samplesCount;
                            mAnimator.set((mTakenSamplesCount - 1) * k, mTakenSamplesCount * k);
                            progressBar.startAnimation(mAnimator);
                            imageView.setImageDrawable(null);
                            imageView.setImageDrawable(ContextCompat.getDrawable(mView.getViewContext(),
                                    R.drawable.face_success));
                            sendAuthResultProbe(createProbe(res.getTryId(), res.getTryType(), Const.IMAGE_SAMPLES, sample));
                        }

                        if (mTakenSamplesCount == samplesCount) {
                            cameraView.pauseCamera();
                        }
                    }
                });
            }

            @Override
            public void onFaceNotDetected() {
                if (mView == null) return;
                Activity activity = (Activity) mView.getViewContext();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageView.setImageBitmap(null);
                        imageView.setImageDrawable(ContextCompat.getDrawable(mView.getViewContext(), R.drawable.face_fail));
                    }
                });
            }
        });
    }

    /**
     * Starts the Fingerprint authenctication
     *
     * @param res {@link AuthRes}
     */
    private void startFpAuth(final AuthRes res) {
        if (mView == null) return;
        final FingerprintFragmentDialog dialog = new FingerprintFragmentDialog();
        AppCompatActivity activity = (AppCompatActivity) mView.getViewContext();
        dialog.show(activity.getSupportFragmentManager(), FingerprintFragmentDialog.class.getSimpleName(), new FingerprintFragmentDialog.OnFingerprintAuth() {
            @Override
            public void onSuccess() {
                sendAuthResultProbe(createProbe(res.getTryId(), res.getTryType(), Const.TOUCH_ID_SAMPLES, "success"));
            }

            @Override
            public void onCanceled(boolean hasEnrolledFps) {
                if (!hasEnrolledFps) {
                    selectAuthOption();
                    dialog.dismiss();
                }
            }

            @Override
            public void onError(int code) {
                dialog.dismiss();
            }
        });
    }

    /**
     * Starts the push button authentication
     *
     * @param res {@link AuthRes}
     */
    private void startPushButtonAuth(final AuthRes res) {
        if (mView == null) return;
        PushView pushView = new PushView(mView.getViewContext());
        pushView.setPushToUnlockListener(new CircleView.OnPushToUnlockListener() {
            @Override
            public void onSuccess() {
                String sample = "success";
//                sendAuthResultProbe(createProbe(res.getTryId(), res.getTryType(), Const., sample));
                selectAuthOption();
            }
        });
        mView.addAuthTypeView(pushView);
    }

    /**
     * Starts the pin code authentication
     *
     * @param res {@link AuthRes}
     */
    private void startPinAuth(final AuthRes res) {
        if (mView == null) return;
        final PinCodeView pinCodeView = new PinCodeView(mView.getViewContext());
        pinCodeView.setupTimer(res);
        mView.addAuthTypeView(pinCodeView);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) pinCodeView.getLayoutParams();
        params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
        params.height = RelativeLayout.LayoutParams.MATCH_PARENT;


        pinCodeView.setPinOkListener(new PinCodeView.OnPinOkListener() {
            @Override
            public void onPinOk() {
                sendAuthResultProbe(createProbe(res.getTryId(), res.getTryType(), Const.PIN_CODE_SAMPLES, "success"));
            }
        });
    }

    /**
     * Converts dp to pixel
     *
     * @param dp dp
     * @return int pixel representation of dp
     */
    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private MsgTry parseTryMessage(String jsonMsg) {
        MsgTry msgTry = new MsgTry();
        try {
            JSONObject json = new JSONObject(jsonMsg);
            JSONObject msgJsonObject = json.getJSONObject("msg");
            JSONArray resourcesJsonArray = msgJsonObject.getJSONArray("resource");

            for (int i = 0; i < resourcesJsonArray.length(); i++) {
                JSONObject res = resourcesJsonArray.getJSONObject(i);
                JSONObject resourceType = res.getJSONObject("resource");
                AuthRes resource = new AuthRes();
                resource.setResourceType(resourceType.getString("rType"));
                resource.setTryType(res.getString("tType"));
                resource.setSamples(res.getInt("samples"));
                resource.setProperties(resourceType.getString("rProperties"));
                msgTry.getResources().add(resource);
            }

            msgTry.setTryId(msgJsonObject.getString("try_id"));
            msgTry.setOid(msgJsonObject.getString("oid"));
            msgTry.setAuthTimeout(msgJsonObject.getInt("authTimeout"));
            JSONArray msgArray = msgJsonObject.getJSONArray("message");
            String msg;
            for (int i = 0; i < msgArray.length(); i++) {
                msg = msgArray.getString(i);
                if (i == 0) {
                    msgTry.setToolbarTittle(msg);
                } else if (i == 1) {
                    msgTry.setMessage(msg);
                } else if (i == 2) {
                    msgTry.setStartAuthButtonMsg(msg);
                }
            }

            JSONObject policy = msgJsonObject.getJSONObject("policy");
            msgTry.setCondition(policy.getString("condition"));

        } catch (JSONException e) {
            Logger.e(TAG, "parseTryMessage " + e.toString());
        }
        return msgTry;
    }

    /**
     * Creates authentication result {@link Probe}
     *
     * @param tryID   current try id
     * @param tryType current try type
     * @param oid     oid
     * @param sample  auth result sample
     * @return {@link Probe}
     */
    private Probe createProbe(String tryID, String tryType, String oid, String sample) {
        Probe probe = new Probe();
        probe.setTryId(tryID);
        probe.setTryType(tryType);
        probe.setStatus("success");
        ProbeData data = new ProbeData();
        data.setOid(oid);
        data.setSample(sample);
        probe.setProbeData(data);
        return probe;
    }

    /**
     * Checks whether app has a camera permissions
     */
    private void checkForCameraPermission() {
        if (!hasCameraPermission()) {
            requestCameraPermission();
        }
    }

    /**
     * Requests camera permission in runtime
     */
    private void requestCameraPermission() {
        if (mView == null) return;
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mView.getViewContext(), Manifest.permission.CAMERA)) {
            showCameraPermissionNotGrantedDialog(mView.getViewContext());
        } else {
            ActivityCompat.requestPermissions((Activity) mView.getViewContext(), new String[]{Manifest.permission.CAMERA}, Const.PERMISSION_CAMERA_RC);
            Logger.d(TAG, "requestCameraPermission :: requesting permissions");
        }
    }

    /**
     * Indicates whether app has a camera permission
     *
     * @return true if has
     */
    private boolean hasCameraPermission() {
        return ContextCompat.checkSelfPermission(mView.getViewContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void showCameraPermissionNotGrantedDialog(final Context context) {
        new AlertDialog.Builder(context)
                .setTitle("Camera permission disabled")
                .setMessage("Camera permission is disabled. Would you like to enable it?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Activity activity = (Activity) context;
                        startAppSettingsActivity(activity);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        sendCanceledProbe();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Starts the app settings activity
     *
     * @param activity {@link Activity}
     */
    private static void startAppSettingsActivity(final Activity activity) {
        if (activity == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + activity.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        activity.startActivityForResult(i, Const.PERMISSION_CAMERA_RC);
    }


}
