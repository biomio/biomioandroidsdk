package com.biomio.sdk.mvp;


import com.biomio.sdk.adapter.AuthOptionsAdapter;
import com.biomio.sdk.model.AuthRes;

import core.Probe;

public interface BiomioPresenter extends AuthOptionsAdapter.OnAuthTypeSelectedListener {


    String ACTION_AUTH = "actionAuth";
    String ARG_PROBE_STATUS = "argProbeStatus";
    String ARG_AUTH_TRY = "argAuthResources";

    void onResume();

    void onDestroy();

    void startAuth();

    void removeItemFromAdapter(AuthRes item);

    void finishAuth();

    void sendCanceledProbe();

    void registerAuthReceiver();

    void sendAuthResultProbe(Probe probe);

    void release();

}
