package com.biomio.sdk.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.biomio.sdk.R;
import com.biomio.sdk.model.AuthRes;
import com.biomio.sdk.util.Const;


/**
 * {@link android.support.v7.widget.RecyclerView.ViewHolder} which is responsible for
 * representation of active (interactive) auth/registration resources
 */
class AuthTypeActiveVH
        extends BaseAuthVH
        implements View.OnClickListener {

    /**
     * Hosting adaoter
     */
    private AuthOptionsAdapter mAdapter;
    /**
     * ImageView representing {@link AuthRes}
     */
    private ImageView mIbtnAuthType;
    /**
     * ImageButton representing auth success event
     */
    private ImageView mIbtnSuccess;

    AuthTypeActiveVH(AuthOptionsAdapter adapter, View itemView) {
        super(adapter, itemView);
        this.mAdapter = adapter;
        mIbtnAuthType = (ImageView) itemView.findViewById(R.id.ibtn_try);
        mIbtnSuccess = (ImageView) itemView.findViewById(R.id.ibtn_success);
        mIbtnAuthType.setOnClickListener(this);
    }

    /**
     * Binds view with data
     *
     * @param res {@link AuthRes}
     */
    protected void bind(AuthRes res) {
        String authType = res.getTryType();
        if (TextUtils.isEmpty(authType)) return;
        Context context = mIbtnAuthType.getContext();
        mIbtnAuthType.setImageDrawable(null);
        switch (authType) {
            case Const.AUTH_FACE:
                mIbtnAuthType.setImageDrawable(getDrawable(context, R.drawable.try_face));
                break;
            case Const.AUTH_PUSH:
                mIbtnAuthType.setImageDrawable(getDrawable(context, R.drawable.try_push_button));
                break;
            case Const.AUTH_FP:
                mIbtnAuthType.setImageDrawable(getDrawable(context, R.drawable.try_fp));
                break;
            case Const.AUTH_PIN_CODE:
                mIbtnAuthType.setImageDrawable(getDrawable(context, R.drawable.try_pass));
                break;
        }
        mIbtnAuthType.setTag(res);
        markAuthSuccessful(res);
        markAuthSelected(res);
    }

    /**
     * Marks {@link AuthRes} as successful by Swapping visibility of views from layout
     *
     * @param authRes {@link AuthRes}
     */
    private void markAuthSuccessful(AuthRes authRes) {
        if (authRes == null) return;
        if (authRes.isPassed()) {
            mIbtnSuccess.setVisibility(View.VISIBLE);
        } else {
            mIbtnSuccess.setVisibility(View.GONE);
        }
    }

    /**
     * Marks {@link AuthRes} as selected by enlighting it with green color
     *
     * @param authRes {@link AuthRes}
     */
    private void markAuthSelected(AuthRes authRes) {
        if (authRes == null) return;
        mIbtnAuthType.setSelected(authRes.isSelected());
    }

    /**
     * Gets Drawable from resources
     *
     * @param context {@link Context}
     * @param id      ResID
     * @return Drawable
     */
    private Drawable getDrawable(Context context, int id) {
        return ContextCompat.getDrawable(context, id);
    }

    /**
     * Handles click event
     *
     * @param v - {@link View}
     */
    @Override
    public void onClick(View v) {
        int vId = v.getId();
        if (vId == R.id.ibtn_try) {
            AuthRes res = (AuthRes) v.getTag();
            if (res != null) {
                res.setSelected(!res.isSelected());
                for (int i = 0; i < mAdapter.getDataList().size(); i++) {
                    AuthRes authRes = mAdapter.getDataList().get(i);
                    if (authRes.equals(res)) {
                        authRes.setSelected(true);
                    } else authRes.setSelected(false);
                }
                mAdapter.notifyDataSetChanged();
                if (mAdapter.mAuthTypeSelectedListener != null) {
                    mAdapter.mAuthTypeSelectedListener.onAuthTypeSelected(res);
                }
            }
        }
    }

}
