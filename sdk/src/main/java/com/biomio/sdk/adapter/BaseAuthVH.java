package com.biomio.sdk.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.biomio.sdk.model.AuthRes;


/**
 * Abstraction of auth view holder
 */
abstract class BaseAuthVH extends RecyclerView.ViewHolder {

    BaseAuthVH(AuthOptionsAdapter adapter, View itemView) {
        super(itemView);
    }

    /**
     * Binds data with view
     *
     * @param res {@link AuthRes}
     */
    protected abstract void bind(AuthRes res);
}
