package com.biomio.sdk.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biomio.sdk.R;
import com.biomio.sdk.model.AuthRes;
import com.biomio.sdk.util.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter responsible for representation of {@link AuthRes} - auth resources received
 * for authentication or registration
 */
public class AuthOptionsAdapter extends RecyclerView.Adapter<BaseAuthVH> {


    private static final String TAG = AuthOptionsAdapter.class.getSimpleName();
    /**
     * Interactive resource
     */
    private static final int TYPE_ACTIVE = 1;
    /**
     * Non interactive resource
     */
    private static final int TYPE_PASSIVE = 2;
    /**
     * List of resources
     */
    private List<AuthRes> mDataList = new ArrayList<>();

    /**
     * Listener designated to notify about click event of {@link AuthRes}
     */
    OnAuthTypeSelectedListener mAuthTypeSelectedListener;

    public AuthOptionsAdapter(List<AuthRes> authOptionsList) {
        this.mDataList = authOptionsList;
    }

    @Override
    public BaseAuthVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        BaseAuthVH vh;
        if (viewType == TYPE_ACTIVE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_auth_type_active, parent, false);
            vh = new AuthTypeActiveVH(this, view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_location, parent, false);
            vh = new AuthTypePassiveVH(this, view);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(BaseAuthVH holder, int position) {
        if (position > mDataList.size()) return;
        holder.bind(mDataList.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position < mDataList.size()) {
            AuthRes res = mDataList.get(position);
            if (res.isActive()) return TYPE_ACTIVE;
            else return TYPE_PASSIVE;
        }
        return TYPE_ACTIVE;
    }

    /**
     * Removes item from adapter
     *
     * @param item {@link AuthRes}
     */
    public void removeItem(AuthRes item) {
        try {
            int position = mDataList.indexOf(item);
            Logger.d(TAG, "removeItem :: position " + position);
            mDataList.remove(position);
            notifyItemRemoved(position);
        } catch (Exception e) {
            Logger.e(TAG, "removeItem ::  " + e.toString());
        }
    }

    /**
     * Adds item to adapter
     *
     * @param item {@link AuthRes}
     */
    public void addItem(AuthRes item) {
        try {
            mDataList.add(item);
            int position = mDataList.indexOf(item);
            notifyItemInserted(position);
        } catch (Exception e) {
            Logger.e(TAG, "addItem ::  " + e.toString());
        }
    }

    /**
     * Notifies adapter about item change event
     *
     * @param authType {@link AuthRes}
     */
    public void itemChanged(AuthRes authType) {
        int index = mDataList.indexOf(authType);
        notifyItemChanged(index);
    }

    /**
     * Returns container of {@link AuthRes}
     *
     * @return {@link AuthRes}
     */
    public List<AuthRes> getDataList() {
        return mDataList;
    }

    /**
     * Listener designated to notify about click event of {@link AuthRes}
     */
    public interface OnAuthTypeSelectedListener {
        void onAuthTypeSelected(AuthRes authRes);
    }

    /**
     * Registers {@link OnAuthTypeSelectedListener}
     *
     * @param authTypeSelectedListener {@link OnAuthTypeSelectedListener}
     */
    public void setAuthTypeSelectedListener(OnAuthTypeSelectedListener authTypeSelectedListener) {
        this.mAuthTypeSelectedListener = authTypeSelectedListener;
    }
}
