package com.biomio.sdk.adapter;

import android.view.View;
import android.widget.ProgressBar;

import com.biomio.sdk.R;
import com.biomio.sdk.model.AuthRes;

/**
 * This {@link android.support.v7.widget.RecyclerView.ViewHolder} is responsible for
 * representing non interactive (passive) auth/registration resource types
 * such as location
 */
class AuthTypePassiveVH extends BaseAuthVH {

    /**
     * Progress bar which shows progress of location update
     */
    private ProgressBar mPb;

    AuthTypePassiveVH(AuthOptionsAdapter authOptionsAdapter, View view) {
        super(authOptionsAdapter, view);
        this.mPb = (ProgressBar) itemView.findViewById(R.id.pb_non_interactive);
    }

    @Override
    protected void bind(AuthRes res) {
        if (res.isSelected()) {
            showPB();
        } else {
            hidePB();
        }
    }

    /**
     * Show {@link ProgressBar}
     */
    private void showPB() {
        mPb.setVisibility(View.VISIBLE);
    }

    /**
     * Hides {@link ProgressBar}
     */
    private void hidePB() {
        mPb.setVisibility(View.INVISIBLE);
    }

}
